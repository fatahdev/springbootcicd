file=$(ls /home/app/javaTest/*.jar)

if [[ ! -z $file ]] ;
then
        fileName=$(basename -- $file)
        echo $fileName

        ps -ef | grep $fileName | grep -v grep | awk '{print $2}' | xargs sudo kill -9

        rm -R /home/app/javaTest/$fileName

        mv /home/app/javaTest/tmp/$1 /home/app/javaTest/

        sudo nohup java -jar /home/app/javaTest/$1  > /home/app/javaTest/log.txt 2>&1 &
else
        echo "Ga Ada"
        mv /home/app/javaTest/tmp/$1 /home/app/javaTest/
        sudo nohup java -jar /home/app/javaTest/$1  > /home/app/javaTest/log.txt 2>&1 &
fi
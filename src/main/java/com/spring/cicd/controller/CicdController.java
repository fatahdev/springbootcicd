package com.spring.cicd.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CicdController {

    @GetMapping("/test")
    public ResponseEntity<?> testController(){
        return ResponseEntity.ok("Testing Endpoint");
    }
}
